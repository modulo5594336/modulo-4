import app from './app.js';
import { sequelize } from './database/database.js';
import 'dotenv/config';
import logger from './logs/logger.js';

async function main() {
  console.clear();
  await sequelize.sync({ force: false });
  
  app.listen(5432);
  logger.info(`Server on port ${5432}`);
  logger.error('Server on port ');
  logger.debug('Server on port ');
  logger.warn('Server on port ');
  logger.fatal('Server on port ');
}

main();