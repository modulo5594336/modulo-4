import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Category } from './Category.js';
import { Products } from './Products.js';

export const User = sequelize.define(
  'usuarios',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    correo: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    contrasena: {
      type: DataTypes.STRING,
      required: true,
    },
    estado: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    timestamps: false,
  }
);

User.hasMany(Products, {
  foreignKey: 'usuario_id',
  sourceKey: 'id',
});

Products.belongsTo(User, {
  foreignKey: 'usuario_id',
  targetKey: 'id',
});

User.hasMany(Category, {
  foreignKey: 'usuario_id',
  sourceKey: 'id',
});

Category.belongsTo(User, {
  foreignKey: 'projectId',
  targetKey: 'id',
});