import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Products } from './Products.js';
import { User } from './User.js';

export const Category = sequelize.define('categorias', {

    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
    nombre: {
    type: DataTypes.STRING,
    allowNull: false,
  },

});

Category.hasMany(Products, {
    foreignKey: 'categoria_id',
    sourceKey: 'id',
  });
  
  Products.belongsTo(Category, {
    foreignKey: 'categoria_id',
    targetKey: 'id',
  });