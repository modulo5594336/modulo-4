import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Category } from './Category.js';
import { User } from './User.js';

export const Products = sequelize.define('productos', {

    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
    nombre: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  precio_unitario: {
    type: DataTypes.NUMBER,
    allowNull: false,
  },
  estado: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  },
});

