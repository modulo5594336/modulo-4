import { User } from '../models/User.js';
import { Category } from '../models/Category.js';
import { Products } from '../models/Products.js';

export async function getCategories(req, res) {
  try {
    const categories = await Category.findAll({
      attributes: ['id', 'nombre', 'usuario_id'],
    });

    return res.json(categories);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function createCategory(req, res) {
  console.log('Creating category', req.body);
  const { nombre } = req.body;
  try {
    const newCategory = await Category.create(
      {
        nombre,
      }, 
      {
        fields: ['nombre'],
      }
    );
    return res.json(newCategory);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getCategory(req, res) {
  const { id } = req.params;
  try {
    const category = await Category.findOne({
      where: { id },
    });
    return res.json(category);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function updateCategory(req, res) {
  const { id } = req.params;
  const { nombre } = req.body;

  try {
    const category = await Category.findByPk(id);
    category.nombre = nombre;
    
    await category.save();

    return res.json(category);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function deleteCategory(req, res) {
  const { id } = req.params;
  try {
    
    await Products.destroy({
        where: { categoria_id: id },
      });
    await Category.destroy({
      where: { id },
    });
    return res.sendStatus(204);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}