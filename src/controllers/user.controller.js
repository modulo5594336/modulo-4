import { User } from '../models/User.js';
import { Category } from '../models/Category.js';
import { Products } from '../models/Products.js';

export async function getUsers(req, res) {
  try {
    const users = await User.findAll({
      attributes: ['id', 'nombre', 'correo', 'contrasena', 'estado'],
    });

    return res.json(users);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function createUser(req, res) {
  console.log('Creating user', req.body);
  const { nombre, correo, contrasena, estado } = req.body;
  try {
    const newUser = await User.create(
      {
        nombre,
        correo,
        contrasena,
        estado,
      },
      {
        fields: ['nombre', 'correo', 'contrasena','estado' ],
      }
    );
    return res.json(newUser);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getUser(req, res) {
  const { id } = req.params;
  try {
    const user = await User.findOne({
      where: { id },
    });
    return res.json(user);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function updateUser(req, res) {
  const { id } = req.params;
  const { nombre, correo, contrasena, estado } = req.body;

  try {
    const user = await User.findByPk(id);
    user.nombre = nombre;
    user.correo = correo;
    user.contrasena = contrasena;
    user.estado = estado;

    await user.save();

    return res.json(user);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function deleteUser(req, res) {
  const { id } = req.params;
  try {
    await Category.destroy({
      where: { usuario_id: id },
    });
    await Products.destroy({
        where: { usuario_id: id },
      });
    await User.destroy({
      where: { id },
    });
    return res.sendStatus(204);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

