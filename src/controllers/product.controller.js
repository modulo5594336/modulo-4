import { User } from '../models/User.js';
import { Category } from '../models/Category.js';
import { Products } from '../models/Products.js';

export async function getProducts(req, res) {
  try {
    const products = await Products.findAll({
      attributes: ['id', 'nombre', 'usuario_id', 'categoria_id', 'estado', 'precio_unitario'],
    });

    return res.json(products);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function createProduct(req, res) {
    console.log('Creating product', req.body);
    const { nombre, estado , precio_unitario} = req.body;
    try {
      const newProduct = await Products.create(
        {
          nombre,
          precio_unitario,
          estado,
        },
        {
          fields: ['nombre', 'precio_unitario','estado' ],
        }
      );
      return res.json(newProduct);
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

export async function getProduct(req, res) {
  const { id } = req.params;
  try {
    const product = await Products.findOne({
      where: { id },
    });
    return res.json(product);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function updateProduct(req, res) {
  const { id } = req.params;
  const { nombre, estado , precio_unitario} = req.body;

  try {
    const product = await Products.findByPk(id);
    product.nombre = nombre;
    product.precio_unitario = precio_unitario;
    product.estado = estado;
    
    await product.save();

    return res.json(product);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function deleteProduct(req, res) {
  const { id } = req.params;
  try {
    await Products.destroy({
      where: { id },
    });
    return res.sendStatus(204);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}