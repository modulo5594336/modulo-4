import { Sequelize } from 'sequelize';

const Sequelize = require('sequelize');
const { connect } = require('pg');


export const db = new Sequelize({
  dialect: 'postgres',
  host: 'localhost',
  port: 5432,
  database: 'proyecto',
  username: 'postgres',
  password: 'postgres',
});

db.sync({ force: true }).then(() => {
  console.log('Base de datos creada correctamente');
});