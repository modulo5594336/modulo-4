import { Router } from 'express';
import {
  getCategories,
  createCategory,
  getCategory,
  updateCategory,
  deleteCategory,
} from '../controllers/category.controller.js';

const router = Router();

router.get('/', getCategories);

router.post('/', createCategory);

router.get('/:id', getCategory);

router.put('/:id', updateCategory);

router.delete('/:id', deleteCategory);


export default router;