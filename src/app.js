import express from 'express';
import morgan from 'morgan';
const jwt = require('jsonwebtoken');
const app = express();

import userRoutes from './routes/user.route.js';
import productRoutes from './routes/product.route.js';
import categoryRoutes from './routes/category.routes.js';

app.use(morgan('dev'));
app.use(express.json());

const jwt = require('express-jwt');


app.use(jwtMiddleware({
  secret: 'Key',
  algorithms: ['HS256'],
}));


  
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/categories', categoryRoutes);



export default app;